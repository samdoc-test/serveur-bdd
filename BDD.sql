CREATE DATABASE IF NOT EXISTS dbSuivi;
--La base de données doit s'appeller dbSuivi
--A modifier par la suite, il s'agit d'un utilisateur et un mot de passe pour tester les applications
--Si votre base de données est executé sur le même serveur que les serveurs webs
DROP USER 'utilisateur-exemple'@localhost;
CREATE USER 'utilisateur-exemple'@localhost IDENTIFIED BY '=N:Pf$|E5M_D863`';
--Si votre base de données n'est pas executé sur le même serveur que les serveurs webs
DROP USER 'utilisateur-exemple';
CREATE USER 'utilisateur-exemple' IDENTIFIED BY '=N:Pf$|E5M_D863`';
GRANT SELECT on * TO 'utilisateur-exemple';
FLUSH PRIVILEGES;

drop table MALADIE_ATTRAPEE;
drop table MEDICAMENT;
drop table MALADIE;
drop table PATIENT;
create table PATIENT
(
	PA_ID int not null,
	PA_NOM char(32) not null,
	PA_PRENOM char(32) not null,
	PA_DATE_NAISSANCE date not null,
	PA_GENRE char(1) not null,
    CONSTRAINT PK_PATIENT PRIMARY KEY (PA_ID)  
);
create table MALADIE
(
	MA_ID int not null,
	MA_NOM char(32) not null,
    CONSTRAINT PK_MALADIE PRIMARY KEY (MA_ID)  
);
create table MEDICAMENT
(
	ME_ID int not null,
	ME_NOM char(32) not null,
    CONSTRAINT PK_MEDICAMENT PRIMARY KEY (ME_ID)  
);
create table MALADIE_ATTRAPEE
(
	PA_ID int not null,
	MA_ID int not null,
	AT_DATE date not null,
	ME_ID int null,
    MAAT_GUERRIS boolean not null,
    CONSTRAINT PK_MALADIE_ATTRAPEE PRIMARY KEY (PA_ID,MA_ID,AT_DATE)  
);

ALTER TABLE MALADIE_ATTRAPEE ADD (
     CONSTRAINT FK_MALADIE_ATTRAPEE_PA_ID
          FOREIGN KEY (PA_ID)
               REFERENCES PATIENT (PA_ID));

ALTER TABLE MALADIE_ATTRAPEE ADD (
	CONSTRAINT FK_MALADIE_ATTRAPEE_MA_ID
		FOREIGN KEY (MA_ID)
			REFERENCES MALADIE (MA_ID));

ALTER TABLE MALADIE_ATTRAPEE ADD (
	CONSTRAINT FK_MALADIE_ATTRAPEE_ME_ID
		FOREIGN KEY (ME_ID)
			REFERENCES MEDICAMENT (ME_ID));

insert into PATIENT values (0,'Bern','Stephane',STR_TO_DATE('14/11/1961','%d/%m/%Y'),'M');
insert into PATIENT values (1,'Plaza','Stephane',STR_TO_DATE('9/06/1971','%d/%m/%Y'),'M');
insert into PATIENT values (2,'Duval','Sandra',STR_TO_DATE('18/04/1981','%d/%m/%Y'),'F');
insert into PATIENT values (3,'Forest','Lucie',STR_TO_DATE('14/11/1991','%d/%m/%Y'),'F');
insert into PATIENT values (4,'Le boulanger','Catherine',STR_TO_DATE('12/08/2001','%d/%m/%Y'),'F');
insert into PATIENT values (5,'Soleil','Titouan',STR_TO_DATE('01/01/2021','%d/%m/%Y'),'M');
insert into PATIENT values (6,'Smith','John',STR_TO_DATE('29/09/2011','%d/%m/%Y'),'M');
insert into PATIENT values (7,'Valentini','Camille',STR_TO_DATE('27/12/1991','%d/%m/%Y'),'F');
insert into PATIENT values (8,'Leprince','Sofia',STR_TO_DATE('31/01/1999','%d/%m/%Y'),'F');
insert into PATIENT values (9,'Tomassin','Pierrick',STR_TO_DATE('07/06/1981','%d/%m/%Y'),'M');

insert into MALADIE values (0,'ECZEMA');
insert into MEDICAMENT values (0,'CORTICOSTEROIDS');
insert into MEDICAMENT values (1,'ANTIHISTAMINES');
insert into MALADIE values (1,'CANCER');
insert into MALADIE values (2,'MOLLUSCOM');
insert into MEDICAMENT values (2,'CANTHARIDINE');
insert into MALADIE values (3,'MALADIE DE LYME');
insert into MEDICAMENT values (3,'CEFTRIAXONE');
insert into MEDICAMENT values (4,'CEFUROXIME');
insert into MEDICAMENT values (5,'AMOXICILLINE');
insert into MALADIE values (4,'GRIPPE');
insert into MEDICAMENT values (6,'PERAMIVIR');
insert into MEDICAMENT values (7,'OSELTAMIVIR');
insert into MALADIE values (5,'COVID-19');

insert into MALADIE_ATTRAPEE values(0,0,STR_TO_DATE('14/11/1999','%d/%m/%Y'),0,true);
insert into MALADIE_ATTRAPEE values(0,4,STR_TO_DATE('28/03/2010','%d/%m/%Y'),null,true);
insert into MALADIE_ATTRAPEE values(4,2,STR_TO_DATE('07/09/2010','%d/%m/%Y'),2,true);
insert into MALADIE_ATTRAPEE values(2,4,STR_TO_DATE('19/12/2018','%d/%m/%Y'),6,true);
insert into MALADIE_ATTRAPEE values(1,0,STR_TO_DATE('26/11/2012','%d/%m/%Y'),1,true);
insert into MALADIE_ATTRAPEE values(6,3,STR_TO_DATE('31/08/2016','%d/%m/%Y'),3,false);
insert into MALADIE_ATTRAPEE values(8,5,STR_TO_DATE('02/04/2020','%d/%m/%Y'),null,true);

insert into MALADIE_ATTRAPEE values(0,5,STR_TO_DATE('14/01/2021','%d/%m/%Y'),null,false);
insert into MALADIE_ATTRAPEE values(0,5,STR_TO_DATE('12/01/2021','%d/%m/%Y'),null,false);
insert into MALADIE_ATTRAPEE values(4,5,STR_TO_DATE('13/01/2021','%d/%m/%Y'),null,false);
insert into MALADIE_ATTRAPEE values(2,5,STR_TO_DATE('10/01/2021','%d/%m/%Y'),null,false);
insert into MALADIE_ATTRAPEE values(1,5,STR_TO_DATE('12/01/2021','%d/%m/%Y'),null,false);
insert into MALADIE_ATTRAPEE values(6,5,STR_TO_DATE('15/01/2021','%d/%m/%Y'),null,false);
insert into MALADIE_ATTRAPEE values(8,5,STR_TO_DATE('14/01/2021','%d/%m/%Y'),null,false);
commit;