# serveur-bdd

Ce fichier regroupe toute les tables ainsi qu'un utilisateur-exemple pour vous connecter à la base de donnée. Votre base de donné doit s'appeler `dbSuivi`.
Les tables utilise des type standard. Il est recommandée d'utiliser l'utilisateur exemple ou de créer un nouvel utilisateur pour plus de sécurité.

## Installation
Si vous avez une base de donnée mysql, il est inutile d'installer mariadb étant donné que les deux systèmes de base de donnée sont très proche.
Installer Mariadb depuis le [site officiel](https://mariadb.com/downloads/) sur Windows ou Mac Os.

Sur linux, tapez dans la console `sudo apt install mariadb-server` puis `sudo mysql_secure_installation`.


Procéder à l'installation de Mariadb. Pensez à noter le mot de passe, il vous servira à créer les tables.

### Méthode console (recommandée)
Sur windows, ouvrez le raccourci `MySQL Client (MariaDB 10.5 (x64))` qui c'est crée dans votre menu démarrer. Sur linux, tapez `$ sudo mysql -u root`.

Entrez votre mot de passe. Une fois cela fait, copiez le contenu du fichier dans la console puis coller le dans la console. Normalement, il faudra appuyer une fois sur Entrer et la base de donnée ainsi que ces tables seront crée.

### Méthode SQLDevelopper
Pour installer la base donnée avec SQLDevelopper, il faut d'abord créer la base de donnée. Pour cela, suivez les instructions dans la méthode console ou PHPMyAdmin et éxécutez uniquement la première ligne.

Télécharger le [connecteur J de mySQL](https://dev.mysql.com/downloads/connector/j/) (celui de MariaDB semble ne pas fonctionnée). Sélectionner platform independent. Dans le fichier compréssé, vous trouverez un fichier jar, extrayez le. Ouvrez SQLDevelopper et aller dans Outils > Préférences > Base de données > Pilotes JDBC tiers > Ajouter une entrée. Sélectionner alors le fichier jar. Redémarrez sqlDevelopper.

Cliquez sur + et sélectionné le type de base de donné MySQL, rentrez les informations nécéssaires et sélectionné la base de données dbSuivi. Ouvrez le fichier .sql et éxécutez le.
### Méthode PHPMyAdmin
Connecter vous avec l'identifiant root et le mot de passe que vous avez saisi précédemment.
Cliquer sur Nouvelle base de données. Rentrez comme nom dbSuivi et sélectionner de préférence : `utf8_general_ci`.

Ensuite, cliquer sur la base de donné dbSuivi puis cliquez sur SQL. Copiez toutes les lignes à partir de la ligne 2 et collez les dans l'espace dédié. Une fois cela fait, cliquez sur Exécuter. 

## Connexion
Le fichier SQL crée automatiquement un utilisateur nommée `utilisateur-exemple` avec pour mot de passe =N:Pf$|E5M_D863\`

Il est recommandée de changer le mot de passe pour plus de sécurité.

## Liste des tables

| PATIENT |
| ------ |
| **PA_ID** : int |
| PA_NOM : char(32) |
| PA_PRENOM : char(32) |
| PA_DATE_NAISSANCE : date |
| PA_GENRE : char(1) |

| MALADIE |
| ------ |
| **MA_ID** : int |
| MA_NOM : char(32) |

| MEDICAMENT |
| ------ |
| **ME_ID** : int |
| ME_NOM : char(32) |

| MALADIE_ATTRAPEE |
| ------ |
| _**PA_ID**_ : int |
| _**MA_ID**_ : int |
| _**AT_DATE**_ : int |
| _ME_ID_ : int null |
| MAAT_GUERRIS : boolean |

## Contexte
Ce projet est un test technique demandé par samdoc pour ma demande de stage. Ce projet utilise une base de donnée que vous pouvez trouvez sur la page du groupe.
